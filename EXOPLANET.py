import sys,os
import matplotlib.pyplot as plt
import analytic_radcon as AN_RC
import numpy as np
import math
import scipy

# R = 1.5
# I = 0.4
# lum = 0.1

R = AN_RC.R
I = AN_RC.I

####################     import values from AN_RC     ####################
lum = AN_RC.lum                           #stellar luminosity
metallicity = 1                             #yeah :)
au = (lum/I)**0.5                           #semimajor axis
yr = (au**3/0.965)**0.5                     #orbital period
p0 = AN_RC.p0                               #surface pressure [bar]
T0 = AN_RC.T0                               #surface temp [K]
Ttoa = AN_RC.Ttoa                           #temp at stratopause [K]
Fs = AN_RC.Fs                               #flux absorbed in stratosphere [W/m**2]
Ft = AN_RC.Ft                               #flux absorbed in troposphere [W/m**2]
Ft0 = AN_RC.Ft0                             #shortwave flux  absorbed in troposphere (excluding surface) [W/m**-2]
Fi = AN_RC.Fi                               #internal heat flux [W/m**2]
ks = AN_RC.ks                               #stratospheric attenuation parameter (channel 1)
kt = AN_RC.kt                               #tropospheric attenuation parameter (channel 2)
gam = 1.4                                   #ratio of specific heats, cp/cv
alpha = 0.81                                #adjustment to dry adiabatic lapse rate
n = 2.                                      #power for optical depth - pressure scaling (tau~p**n)
sigma = 5.67E-8                             #Stefan-Boltzmann constant, J/s/m**2/K**4
D = 1.66                                    #diffusivity factor (see Rodgers & Walshaw, 1966)
tau0 = AN_RC.x[1]                           #total thermal optical depth of atmosphere
taurc = AN_RC.x[0]                          #optical depth at radiative-convective boundary

'''can switch these print statements on and off, depending on if you want to see your initial input'''
# print("Semimajor axis [au]: " + str(au))
# print("Orbital period [yr]: " + str(yr))
# print("Surface temperature [K]: " + str(T0))
# print("Temp at stratopause [K]: " + str(Ttoa))
# print("Flux absorbed in stratosphere [W/m**2] [K] - Fs: " + str(Fs))
# print("Flux absorbed in troposphere [W/m**2] -Ft: " + str(Ft))
# print("Internal heat flux [W/m**2] - Fi: " + str(Fi))

'''now run RC solver for all input variables – see analytic_radcon.py for details'''
AN_RC.rcsolver(p0, tau0, Fs, Ft, Fi, ks, kt, gam, alpha, n)

if AN_RC.ks < 1E-5:
    AN_RC.ks = 1E-5
if AN_RC.kt < 1E-5:
    AN_RC.kt = 1E-5

#################### PRINT RESULTS ####################
# print("#######################################################\n"
#       "RESULTS\n"
#       "#######################################################")
# print("Gray thermal optical depth of R-C Boundary: " + str(x[0]))
# print("Gray thermal optical depth of atmosphere: " + str(x[1]))
# print("Attenuation Parameter for Channel 1: " + str(ks))
# print("Attenuation Parameter for Channel 2: " + str(kt))
# print("Semimajor axis [au]: " + str(au))
# print("Orbital period [yr]: " + str(yr))
# print("Surface temperature [K]: " + str(T0))
# print("Temp at stratopause [K]: " + str(Ttoa))
# print("Flux absorbed in stratosphere [W/m**2] [K] - Fs: " + str(Fs))
# print("Flux absorbed in troposphere [W/m**2] -Ft: " + str(Ft))
# print("Internal heat flux [W/m**2] - Fi: " + str(Fi))



#################### GENERATE P-T PROFILE ####################
'''starting with: grid for gray optical depth'''
taumin = 1E-4                       #minimum value of optical depth
taumax = AN_RC.x[1]                 #optical depth at reference pressure p0
lvls = 100                          #number of points
dlntau = ((math.log(taumax)) - (math.log(taumin)))/(lvls-1)    #divisions in log space of tau
tau = np.zeros([lvls])            #array of Nlvls points for logarithm to base 10 of optical depth; initialized to zero
tau_i = 0                         #initialize array of log optical depth at zero
tau[lvls-1] = math.log(taumax)    #assign max value of log optical depth

for num in range(lvls-2, -1, -1):
    tau[num] = tau[num + 1] - dlntau     #generate array
tau = np.exp(tau)



'''corresponding pressure grid'''
p = (((p0*(tau/tau0))**(1/n)))


'''temperature profile, initialized to zero'''
T = p                               #sets size and type of temp array equivalent to pressure array
T = [0] * T                         #then zero out the temperature array

'''temperature in radiative region (RC12 eq. 18), which only applies
for optical depths smaller than taurc (i.e., above the R-C boundary)'''
sigma = 5.67E-8                     #Stefan-Boltzmann constant
D = 1.66                            #diffusivity factor


sigmaT = (Fs/2*(1 + D/ks + (ks/D - D/ks)*np.exp(-(ks*tau)))) + \
         (Ft/2*(1 + D/kt + (kt/D - D/kt)*np.exp(-(kt*tau)))) + \
         (Fi/2*(1 + D*tau))

iRAD = np.where(tau <= taurc)
iRAD = np.shape(100)   #set temperatures in radiative regime

T[iRAD] = ((sigmaT/sigma)**(0.25))
'''temperature in radiative region (RC12 eq. 18), which only applies
for optical depths larger than taurc (i.e., below the R-C boundary)'''
beta = alpha*(gam - 1.)/gam         #beta defined in eq. 11 of RC12
iCON = np.where(tau > taurc)        #find array subscripts for convective regime
#T[iCON] = T0*(tau[iCON]/tau0)**(beta/n) set temperatures for convective regime
T[iCON] = ((T0*(tau[iCON]/tau0)**(beta/n)))

T[iCON] = T[iCON] + (T[np.argmax(T[iCON])] - T[np.argmin(T[iCON])])
# print(T1)
# print(T2)

a = ((p[np.argmin(iCON)+1]) - p[np.argmin(iCON)])/((np.argmin(iCON)+1) - (np.argmin(iCON)))
b = (p[np.argmin(iCON)] - (a*T)[np.argmin(iCON)])
c = T[np.argmax(T[iRAD])] - (p[np.argmax(T[iRAD])]-b)/a
T[iCON] = T[iCON] + c
#iCON for T2 (T[iCON]), iRAD for T1 (T[iRAD])
tautp = math.log(Fs*((ks/D)**2-1)/(Ft + Fi))/ks
tauDIFF = abs(tau-tautp)
iDIFF = np.where(tauDIFF == min(abs(tau-tautp)))
# print(tautp)
# print(tauDIFF)
# print(iDIFF)

print("Gray thermal optical depth at tropopause: " ,tau[iDIFF])
print("Pressure at tropopause [bar]: " ,p[iDIFF])
print("Temperature at tropopause [K]: " ,T[iDIFF])


# **Check that pressure and temp arrays are same size**
'''plot T-P profile'''

fig, ax = plt.subplots()
for kk in range(0,1):
    if kk == 0:
        plt.xlim(30,350)
        plt.ylim(5,0.001)
        plt.yscale('log')
        plt.xlabel("Temperature [K]")
        plt.ylabel("Pressure [bar]")
        ax.plot(T,p, color='black')
    # plt.show()
print('T')
print(T)
Tpoints = list(range(1,5000))
Tpoints = np.array(Tpoints)

# H2S + Zn -> ZnS + H2
PpointsZnS = np.exp(21.46 - 37.98E3/Tpoints) * (4.98E11/metallicity**2)
PpointsZnS2 = 10**(((12.52 - 1E4)/(Tpoints - 1.26*math.log10(metallicity)))/0.63)

# CO + 3H2 -> CH4 + H2O
PpointsCOCH4 = (np.exp(25.79 - 24.8E3/Tpoints)/(1111.45/metallicity))**0.5

# N2 + 3H2 -> 2NH3
PpointsN2NH3 = (np.exp(23.82 - 11.05E3/Tpoints)/(15113.86/metallicity))**0.5

# WATER H2O
PpointsH2O = 10**((38.84 - 3.93*math.log10(metallicity) - 1E4/Tpoints)/(3.83 + 0.20*math.log10(metallicity)))
PpointsH2Ogl = 6.11657E-3*np.exp(45.051E3/8.314*(1/273.16 - 1/Tpoints))/(7.71E-4*metallicity)
PpointsH2Ogs = 6.11657E-3*np.exp(51.059E3/8.314*(1/273.16 - 1/Tpoints))/(7.71E-4*metallicity)

# AMMONIA NH3
PpointsNH3gl = 6.06E-2 *np.exp(16.4E3/8.314*((1/195.4) - (1/Tpoints)))
PpointsNH3gs = 6.06E-2*np.exp(26.1E3/8.314*((1/195.4) - (1/Tpoints)))
PpointsNH3hot = 4.86886 - (1113.928/(Tpoints-10.409))

# CARBON DIOXIDE CO2
PpointsCO2gl = 5.185*np.exp(23.5E3/8.314*(1/216.58 - 1/Tpoints))
PpointsCO2gs = 5.185*np.exp(31.2E3/8.314*(1/216.58 - 1/Tpoints))

#METHANE CH4
PpointsCH4gl = 1.1169E-1*np.exp(8.6E3/8.314*(1/90.67 - 1/Tpoints))/(4.19E-4*metallicity)
PpointsCH4gs = 1.1169E-1*np.exp(9.7E3/8.314*(1/90.67 - 1/Tpoints))/(4.19E-4*metallicity)


for kk in range(0,1):
    if kk==0:
        plt.xlim(10,1500)
        plt.xlabel('Temperature [K]')
        plt.ylim(1E3, 1E-6)
        plt.yscale('log')
        plt.ylabel('Pressure (bar)')
        plt.plot(Tpoints,PpointsZnS)

        ###CO/CH4
        #plt.plot(Tpoints,PpointsCOCH4)

        ###ZnSs
        # plt.plot(Tpoints, PpointsZnS)
        #plt.plot(Tpoints,ZnS2)

        ###N2/NH3
        #plt.plot(Tpoints,PpointsN2NH3)

        ###H2Os
        #plt.plot(Tpoints,PpointsH2O)
        ax.plot(Tpoints[np.where((Tpoints >= 273.16).any() and (Tpoints <= 647.096).any())], PpointsH2Ogl[np.where((Tpoints >= 273.16).any() and (Tpoints <= 647.096).any())])
        ax.plot(Tpoints[np.where(Tpoints <= 273.16)], PpointsH2Ogs[np.where(Tpoints <= 273.16)])

        ###NH3s
        # plt.plot(Tpoints[np.where(Tpoints >= 194.95 and Tpoints <= 405.4)], PpointsNH3gl[np.where(Tpoints >= 194.95 and Tpoints <= 405.4)])
        # plt.plot(Tpoints[np.where(Tpoints <= 194.95)], PpointsNH3gs[np.where(Tpoints <= 194.95)])

        ###CO2s
        ax.plot(Tpoints[np.where((Tpoints >= 216.58).any() and (Tpoints <= 304.18).any())], PpointsCO2gl[np.where((Tpoints >= 216.58).any() and (Tpoints <= 304.18).any())])
        ax.plot(Tpoints[np.where(Tpoints <= 216.58)], PpointsCO2gs[np.where(Tpoints <= 216.58)])

        ###CH4s
        ax.plot(Tpoints[np.where((Tpoints >= 90.67).any() and (Tpoints <= 190.6).any())], PpointsCH4gl[np.where((Tpoints >= 90.67).any() and (Tpoints <= 190.6).any())])
        ax.plot(Tpoints[np.where(Tpoints <= 90.67)], PpointsCH4gs[np.where(Tpoints <= 90.67)])
        ax.plot(T, p)

    plt.show()