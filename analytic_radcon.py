# import statements
from math import exp
import scipy.special as sc
from scipy import optimize

# function that calls root finder and determines radiative-convective solution
#
# inputs:
#   p0    --  reference pressure (e.g., presssure at surface) (Pa)
#   tau0  --  reference optical depth (at p0)
#   Fs    --  shortwave flux absorbed in stratosphere (W m**-2)
#   Ft    --  shortwave flux absorbed in troposphere (W m**-2)
#   Fi    --  internal heat flux (W m**-2)
#   ks    --  stratospheric attenuation parameter
#   kt    --  tropospheric attenuation parameter
#   gam   --  ratio of specific heats (determines dry adiabatic lapse rate)
#   alpha --  correction to dry adiabatic lapse rate
#   n     --  power for p-tau scaling (tau goes as p**n)
#
# returns:
#   x     -- (taurc,T0) where taurc is optical depth of r-c boundary and T0 is temperature at p0 (K)
#

# User inputs
R = float(input("Enter radius in Earth Radii:" ))
I = float(input("Enter instellation value:" ))
lum = float(input("Enter luminosity:" ))

print("Instellation =" , I)
print("Luminosity =", lum)
print("Semi-major axis [au] =", (lum/I)**0.5)
print("Orbital period [yrs] =", (((lum/I)**0.5)**3/0.965)**0.5)

# model parameters
p0    = 1.0         # surface pressure (Pa)
#tau0  = 1.9         # total thermal optical depth of atmosphere
T0    = (291.52617)*(I**0.20353) #surface temperature [K]
Ttoa  = (269.03825)*(I**0.11932) #temperature at stratopause [K]
Fs    = (6.99977)*(I**0.48546)   #shortwave flux absorbed in stratosphere (W m**2)
Ft    = (233.00156)*(I**1.07175) #shortwave flux absorbed in troposphere (W m**2)
Ft0   = (61.00089)*(I**1.02199)  #shortwave flux  absorbed in troposphere (excluding surface) [W/m**-2]
Fi    = 0.07705+(9.92202e-6*R**5.50992)  # internal heat flux (W m**2)
ks    = 136.949         #stratospheric attenuation parameter (channel 1)
kt    = 0.0359061        #tropospheric attenuation parameter (channel 2)
gam   = 1.4         # ratio of specific heats
alpha = 0.81        # empirical adjustment of dry adiabatic lapse rate
n     = 2.          # power law parameter for tau ~ p**n

print("Surface temperature [K] =", T0)
print("Temperature at stratopause [K] =", Ttoa)
print("Flux absorbed in stratosphere [W/m**2] =", Fs)
print("Flux absorbed in troposphere [W/m**2] =", Ft)
print("Flux absorbed in troposphere excluding the surface [W/m**2] =", Ft0)
print("Internal heat flux [W/m**2] =", Fi)

def rcsolver(p0, T0, Fs, Ft, Fi, ks, kt, gam, alpha, n):

  # important constants
  sigma = 5.67e-8 # stefan-boltzmann constant (W m**-2 K**-4)
  D     = 1.66    # diffusivity factor (see, e.g., Rodgers & Walshaw 1966)

  # additional parameters passed to minimization routine
  pars = (p0, T0, Fs, Ft, Fi, ks, kt, gam, alpha, n, sigma, D)

  # call root finder
  x0  = [0.33,300.] # initial guess for taurc and surface temperature (K)
  sol = optimize.root(rcbound, x0, args = pars)

  print('continuity in [flux, temperature] converged at accuracy: ', sol.qtf)

  # return results
  return sol.x

# function for root-finding algorithm
def rcbound(x,p0,T0,Fs,Ft,Fi,ks,kt,gam,alpha,n,sigma,D):

  # unpack fitted parameters
  taurc, tau0 = x

  # cannot have negative optical depths or temperatures
  if taurc < 0:
    taurc = 0.
  if tau0 < 0:
    tau0 = 1E-5

  # additional arguments used for evaluation
  # p0, tau0, Fs, Ft, Fi, ks, kt, gam, a, n, sigma, D = args

  # beta defined in Eq. 11 of RC12
  beta = alpha*(gam - 1.)/gam

  # upwelling thermal flux at R-C boundary from radiative equilibrium (RC12 Eq. 19)
  # if/else checks if attenuation parameters are zero, avoiding a divide by zero
  Fup_r = Fi/2*(2 + D*taurc)
  if ks != 0:
    Fup_r = Fup_r + Fs/2*(1 + D/ks + (1 - D/ks)*exp(-ks*taurc))
  else:
    Fup_r = Fup_r + Fs/2*(2 + D*taurc)
  if kt != 0:
    Fup_r = Fup_r + Ft/2*(1 + D/kt + (1 - D/kt)*exp(-kt*taurc))
  else:
    Fup_r = Fup_r + Ft/2*(2 + D*taurc) 

  # temperature at R-C boundary from radiative equilibrium (RC12 Eq. 18)
  # same if/else checks as above
  sigmaT_r = Fi/2*(1 + D*taurc)
  if ks != 0:
    sigmaT_r = sigmaT_r + Fs/2*(1 + D/ks + (ks/D - D/ks)*exp(-ks*taurc))
  else:
    sigmaT_r = sigmaT_r + Fs/2*(1 + D*taurc)
  if kt != 0:
    sigmaT_r = sigmaT_r + Ft/2*(1 + D/kt + (kt/D - D/kt)*exp(-kt*taurc))
  else:
    sigmaT_r = sigmaT_r + Ft/2*(1 + D*taurc) 
  T_r      = (sigmaT_r/sigma)**0.25

  # upwelling thermal flux at R-C boundary from convective region (RC12 Eq. 13)
  f = 0. # note: 0 implies solid lower boundary, set equal to 4*beta/n/D/tau0 for diffuse lower boundary
  Fup_c = sigma*T0**4*exp(D*taurc)*((1+f)*exp(-D*tau0) + 1/(D*tau0)**(4*beta/n)*sc.gamma(1+4*beta/n)*(sc.gammaincc(1+4*beta/n,D*taurc) - sc.gammaincc(1+4*beta/n,D*tau0)))

  # temperature at R-C boundary from convective region (RC12 Eq. 11)
  T_c = T0*(taurc/tau0)**(beta/n)

  # return quantities whose differences should be minimized for proper radiative-convective solution
  return [ Fup_r - Fup_c, T_r - T_c]


# call solver
x = rcsolver(p0, T0, Fs, Ft, Fi, ks, kt, gam, alpha, n)


# print results
print('Total thermal optical depth, tau0: ',x[1])
print('Optical depth at radiative-convective boundary, taurc: ',x[0])
print('Pressure at radiative-convective boundary (bar): ',p0*(x[0]/x[1])**(1/n)/1.e5)
